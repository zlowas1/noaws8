#!/bin/bash

echo "Başladı"
# Sonsuz döngü başlat
while true; do
  # Mevcut drive'ları al ve rastgele sırala
  drives=$(rclone listremotes | grep 'x' | shuf)
  for file in *; do
    if [[ $file == *.spt ]]; then
      # Rastgele bir sürücü seç
      selected_drive=$(echo "$drives" | shuf -n 1)
      # Seçilen sürücünün varlığını doğrula
      if rclone about "$selected_drive" >/dev/null 2>&1; then
        # Seçilen sürücünün içeriğini kontrol et
        file_count=$(rclone lsf "$selected_drive" | wc -l)
        if [ "$file_count" -lt 8 ] || [ "$file_count" -eq 0 ]; then
          # Dosya sayısı 8'den az veya hiç dosya yoksa taşı
          echo "Dosya $file, $selected_drive'e taşınıyor..."
          rclone move "$file" "$selected_drive" --drive-upload-cutoff=1000T --drive-acknowledge-abuse --drive-stop-on-upload-limit --drive-chunk-size 1G --fast-list -P
          echo "Dosya $file, $selected_drive'e taşındı."
          # Taşıma bittikten sonra yeniden sürücüleri karıştır
          drives=$(rclone listremotes | grep 'x' | shuf)
        else
          echo "Dosya taşıma için uygun değil, bir sonraki sürücü seçiliyor."
        fi
      else
        echo "Hedef sürücü mevcut değil, bir sonraki sürücü seçiliyor."
      fi
    fi
  done
  # 10 saniye bekleyerek işlemi tekrarla
  sleep 10
done
