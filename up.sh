#!/bin/bash

# Dosya yolu ve chunk boyutu  
FILE_PATH="/zlo/mnt/upload1/*"
CHUNK_SIZE="14G"
CHUNKER_REMOTE="local_chunker" 
SERVICE_ACCOUNT_DIR="/root/acc"
SERVICE_ACCOUNT_COUNT=4000
USED_ACCOUNTS_FILE="/root/used_accounts.txt"
CRYPT_REMOTE="crypt_remote"
PASSWORD="ux4L5dt_o4te50wzsYskFAaX_peb531aK8IN"  # Şifreyi burada belirleyin

# Kullanılmış Service Account'ları takip eden dosya oluştur
touch $USED_ACCOUNTS_FILE

# Gerekli dizinleri oluştur
mkdir -p /zlo/mnt/upload1
mkdir -p /zlo/tmp/zlo
mkdir -p /zlo/tmp/chunks

# Yerel remote'u oluştur
rclone config create local_crypt local

# Chunker remote'u oluştur
rclone config create $CHUNKER_REMOTE chunker remote=local_crypt:/zlo/tmp/chunks chunk_size=$CHUNK_SIZE

# Yeni dosyaları izlemek için inotifywait kullan
inotifywait -m -e create -e moved_to "/zlo/mnt/upload1/" | while read path action file; do
    echo "Yeni Dosya Bulundu: Yeyyyy"

# Dosyayı chunker remote'a taşı
rclone move $FILE_PATH $CHUNKER_REMOTE:/chunks -P

# Crypt remote'u oluştur
rclone config create $CRYPT_REMOTE crypt remote=$CHUNKER_REMOTE:/chunks password=$PASSWORD directory_name_encryption=false

# Parçaları crypt remote'a taşı
rclone move $CHUNKER_REMOTE:/chunks $CRYPT_REMOTE:/encrypted_chunks -P
if [ $? -ne 0 ]; then
  echo "Rclone move komutu başarısız oldu."
  exit 1
fi

# Parçaları /zlo/tmp/zlo dizinine taşı
mv /zlo/tmp/chunks/chunks/encrypted_chunks/* /zlo/tmp/zlo/

# Paylaşım bağlantılarını saklamak için bir dosya oluştur
SHARED_LINKS_FILE="/root/shared_links.txt"
touch $SHARED_LINKS_FILE

# Parçaları farklı Service Account Drive'larına yükle ve paylaş
FILES=$(ls /zlo/tmp/zlo/ | awk '{print $1}')
if [ -z "$FILES" ]; then
  echo "Dosya bulunamadı veya ls komutu başarısız oldu."
  exit 1
fi

for i in $FILES
do
  echo "İşleniyor: $i"
  PART_PATH="/zlo/tmp/zlo/$i"
  echo "Parça yolu: $PART_PATH"

  # Dosya adını kısalt
  SHORT_NAME=$(echo $i | cut -c 1-50)

  # Kullanılmamış bir Service Account bul
  for ((j=1; j<=SERVICE_ACCOUNT_COUNT; j++))
  do
    if ! grep -q "$j.json" $USED_ACCOUNTS_FILE; then
      SERVICE_ACCOUNT_JSON="$SERVICE_ACCOUNT_DIR/$j.json"
      echo "$j.json" >> $USED_ACCOUNTS_FILE
      break
    fi
  done

  # Rclone config ile geçici bir remote oluştur
  rclone config create temp_remote drive service_account_file=$SERVICE_ACCOUNT_JSON

  # Parçayı yükle
  rclone move $PART_PATH temp_remote:/$SHORT_NAME --drive-upload-cutoff=1000T --drive-acknowledge-abuse --drive-stop-on-upload-limit --drive-chunk-size 1G --fast-list --transfers 1 -P

  # Dosya kimliğini al
  FILE_ID=$(rclone link temp_remote:/$SHORT_NAME | grep -oP '(?<=id=)[^&]*')

  # Geçici remote'u sil
  rclone config delete temp_remote

  # Bekleme süresi ekleyin
  sleep 10

  # Python kodunu çalıştırarak dosyayı paylaş
  python3 << EOF
from googleapiclient.discovery import build
from google.oauth2 import service_account

# Servis hesabı kimlik bilgilerini yükleyin
SCOPES = ['https://www.googleapis.com/auth/drive']
SERVICE_ACCOUNT_FILE = '$SERVICE_ACCOUNT_JSON'

credentials = service_account.Credentials.from_service_account_file(
    SERVICE_ACCOUNT_FILE, scopes=SCOPES)
service = build('drive', 'v3', credentials=credentials)

# Paylaşılacak dosyanın ID'sini alın
file_id = '$FILE_ID'

# Paylaşım ayarlarını yapın
anyone_permission = {
    'type': 'anyone',
    'role': 'writer'
}
email_permission = {
    'type': 'user',
    'role': 'writer',
    'emailAddress': 'zlowas1@gmail.com'
}
try:
    service.permissions().create(
        fileId=file_id,
        body=anyone_permission,
        fields='id',
    ).execute()
    service.permissions().create(
        fileId=file_id,
        body=email_permission,
        fields='id',
    ).execute()
    print(f"File {file_id} is now shared with anyone as editor and with zlowas1@gmail.com as editor.")
except Exception as e:
    print(f"Error sharing file {file_id}: {e}")
EOF

done

# Geçici chunk dosyalarını temizle
#rm -rf /zlo/tmp/chunks

echo "Paylaşım bağlantıları $SHARED_LINKS_FILE dosyasına kaydedildi."

done