#!/bin/bash

#Directories to move files to
destinations=(
  "/zlo/mnt/upload1/"    
)

index=0

echo "Starting..."

while true; do
  file=$(inotifywait -q -e create,moved_to --format '%w%f' /zlo/mnt/plot1/)
  if [[ $file == *.spt ]]; then
    mv "$file" "${destinations[$index]}"
    echo "Moved $file to ${destinations[$index]}"
    # Increment index or reset to 0 if we've hit the end of the destinations array
    ((index=(index+1)%${#destinations[@]}))
  fi
done